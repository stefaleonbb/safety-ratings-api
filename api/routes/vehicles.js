const express = require('express');
const router = express.Router();
const axios = require('axios');



// GET http://localhost:8888/vehicles/<MODEL YEAR>/<MANUFACTURER>/<MODEL>
// GET http://localhost:8888/vehicles/<MODEL YEAR>/<MANUFACTURER>/<MODEL>?withRating=true
router.get('/:modelYear/:manufacturer/:model', (req, res, next) => {

    const year = req.params.modelYear;
    const make = req.params.manufacturer;
    const model = req.params.model;    
   
    // respond to malformed requests
    if ( isNaN(year) || !make || !model) {
        res.status(400).json({
            Count: 0,
            Results: []
        });
    }
    
    // fetch the data from the NHTSA API
    const url =
    `https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/${year}/make/${make}/model/${model}?format=json`;

    axios.get(url)
    .then((axiosResult) => {          
        
        // respond to zero results from the NHTSA API        
        if (axiosResult.data.Count === 0) {
            res.status(404).json({
                Count: 0,
                Results: []
            });
        } 

        // respond to non zero results from the NHTSA API 
        const results = [];
        createTheResultsArray(axiosResult.data, results);
        if (req.query.withRating === "true") {
            // respond to the "?withRating=true" requests
            const resultsWithRating = [];
            results.forEach((vehicle) => {

                // fetch the rating data for the specific VehicleId from the NHTSA API
                const vehicleWithRating = {};                                        
                const url = `https://one.nhtsa.gov/webapi/api/SafetyRatings/VehicleId/${vehicle.VehicleId}?format=json`;
                axios.get(url)
                .then((axiosResult) => {                    
                    vehicleWithRating.CrashRating = axiosResult.data.Results[0].OverallRating;  
                    vehicleWithRating.Description = vehicle.Description;
                    vehicleWithRating.VehicleId = vehicle.VehicleId;                        
                    resultsWithRating.push(vehicleWithRating);   
                })
                .then(() => {
                    // respond only after the data for all vehicles in the loop are fetched
                    if (resultsWithRating.length === results.length)
                    {                    
                        res.status(200).json({
                            Count: resultsWithRating.length,
                            Results: resultsWithRating
                        });
                    }
                })
                .catch(() => { res.status(404) });  

            });
        } else {         
            // respond to all the other non-"?withRating=true" requests   
            res.status(200).json({               
                Count: axiosResult.data.Count,
                Results: results
            });    
        }       
        
    })
    .catch(() => { res.status(404) });  // catch bad requests

});     


const createTheResultsArray = (dataFromTheNhtsaApi, resultsArray) => {    
    dataFromTheNhtsaApi.Results.forEach((result) => {
        const vehicle = {};
        vehicle.Description = result.VehicleDescription;
        vehicle.VehicleId = result.VehicleId;
        resultsArray.push(vehicle);                
    });
}     


// POST http://localhost:8888/vehicles
router.post('/', (req, res, next) => {
    
    // respond to erroneous JSON bodies
    if ( isNaN(Number(req.body.modelYear)) || !req.body.manufacturer || !req.body.model) {
        res.status(400).json({
            Count: 0,
            Results: []
        });
    }

    // fetch the data from the NHTSA API
    const url = `https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/${req.body.modelYear}/make/${req.body.manufacturer}/model/${req.body.model}?format=json`;

    axios.get(url)
    .then((axiosResult) => {

        // respond to zero results from the NHTSA API        
        if (axiosResult.data.Count === 0) {
            res.status(404).json({
                Count: 0,
                Results: []
            });
        } 

        // respond to non zero results from the NHTSA API 
        const results = [];
        createTheResultsArray(axiosResult.data, results);        
        res.status(200).json({               
            Count: axiosResult.data.Count,
            Results: results
        });

    })   
    .catch(() => { res.status(404) }); 
    
});



module.exports = router;