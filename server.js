const app = require('./app');
const port = process.env.PORT || 8888;

app.listen(port, process.env.IP, () => {
    console.log(`Server started on port ${port}.`);
});

