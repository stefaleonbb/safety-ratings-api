const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');


const vehiclesRoutes = require('./api/routes/vehicles');


app.use(morgan('dev')); 
app.use(bodyParser.json());


app.use('/vehicles', vehiclesRoutes);


// error handling
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});
app.use((error, req, res, next) => {    
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app;   