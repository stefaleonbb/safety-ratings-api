# Safety Ratings API

Implement an API in Node.js that calls the [NHTSA NCAP 5 Star Safety Ratings API](https://one.nhtsa.gov/webapi/Default.aspx?SafetyRatings/API/5)

## 00 Init

* Using the current LTS Node version (8.11.3), initialize a project and add the dependencies.

`npm init`

`npm install --save express axios body-parser`

`npm install --save-dev morgan`

`package.json`

```
{
  "name": "safetyratingsapi",
  "version": "1.0.0",
  "description": "Create an API that uses the NHTSA NCAP 5 Star Safety Ratings API",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "keywords": [
    "node",
    "api",
    "safetyratings"
  ],
  "author": "stefaleon",
  "license": "MIT",
  "dependencies": {
    "axios": "^0.18.0",
    "body-parser": "^1.18.3",
    "express": "^4.16.3"
  },
  "devDependencies": {
    "morgan": "^1.9.0"
  }
}
```


## 01 Server and App

* Add the server and app files.

`server.js`

```
const app = require('./app');
const port = process.env.PORT || 8888;

app.listen(port, process.env.IP, () => {
    console.log(`Server started on port ${port}.`);
});
```

`app.js`

```
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');


app.use(morgan('dev'));
app.use(bodyParser.json());


// error handling
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});
app.use((error, req, res, next) => {    
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app;   
```

## 02 The vehicles routes

* Create the folder api/routes and add a file for the `/vehicles` routes.

`api/routes/vehicles.js`

```
const express = require('express');
const router = express.Router();
const axios = require('axios');


router.get('/:someParam', (req, res, next) => {
    res.status(200).json({ "this is the GET /vehicles/someParam route": req.params.someParam });
});


router.post('/', (req, res, next) => {
    res.status(200).json({ "this is the POST /vehicles route": req.body.someContent || "body has no content" });
});


module.exports = router;
```

* Use the route in the app.

`app.js`

```
const vehiclesRoutes = require('./api/routes/vehicles');
```
```
app.use('/vehicles', vehiclesRoutes);
```

## 03 Requirement 1

* Respond to the `GET http://localhost:8888/vehicles/<MODEL YEAR>/<MANUFACTURER>/<MODEL>` requests.

`api/routes/vehicles.js`

```
router.get('/:modelYear/:manufacturer/:model', (req, res, next) => {

    const year = req.params.modelYear;
    const make = req.params.manufacturer;
    const model = req.params.model;    

    // respond to malformed requests
    if ( isNaN(year) || !make || !model) {
        res.status(400).json({
            Count: 0,
            Results: []
        });
    }

    // fetch the data from the NHTSA API
    const url =
    `https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/${year}/make/${make}/model/${model}?format=json`;

    axios.get(url)
    .then((axiosResult) => {          

        // respond to zero results from the NHTSA API        
        if (axiosResult.data.Count === 0) {
            res.status(404).json({
                Count: 0,
                Results: []
            });
        }

        // respond to non zero results from the NHTSA API
        const results = [];
        createTheResultsArray(axiosResult.data, results);        
        res.status(200).json({               
            Count: axiosResult.data.Count,
            Results: results
        });

    })
    .catch(() => { res.status(404) });  // catch bad requests

});     

```


```
const createTheResultsArray = (dataFromTheNhtsaApi, resultsArray) => {    
    dataFromTheNhtsaApi.Results.forEach((result) => {
        const vehicle = {};
        vehicle.Description = result.VehicleDescription;
        vehicle.VehicleId = result.VehicleId;
        resultsArray.push(vehicle);                
    });
}        

```


## 04 Requirement 2

* Respond to the `POST http://localhost:8888/vehicles` requests.

`api/routes/vehicles.js`

```
router.post('/', (req, res, next) => {

    // respond to erroneous JSON bodies
    if ( isNaN(Number(req.body.modelYear)) || !req.body.manufacturer || !req.body.model) {
        res.status(400).json({
            Count: 0,
            Results: []
        });
    }

    // fetch the data from the NHTSA API
    const url = `https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/${req.body.modelYear}/make/${req.body.manufacturer}/model/${req.body.model}?format=json`;

    axios.get(url)
    .then((axiosResult) => {

        // respond to zero results from the NHTSA API        
        if (axiosResult.data.Count === 0) {
            res.status(404).json({
                Count: 0,
                Results: []
            });
        }

        // respond to non zero results from the NHTSA API
        const results = [];
        createTheResultsArray(axiosResult.data, results);        
        res.status(200).json({               
            Count: axiosResult.data.Count,
            Results: results
        });

    })   
    .catch(() => { res.status(404) });

});

```


## 05 Requirement 3


* Edit the existing GET route in order to respond to the `GET http://localhost:8888/vehicles/<MODEL YEAR>/<MANUFACTURER>/<MODEL>?withRating=true` requests.

`api/routes/vehicles.js`

```
// GET http://localhost:8888/vehicles/<MODEL YEAR>/<MANUFACTURER>/<MODEL>
// GET http://localhost:8888/vehicles/<MODEL YEAR>/<MANUFACTURER>/<MODEL>?withRating=true
router.get('/:modelYear/:manufacturer/:model', (req, res, next) => {

    const year = req.params.modelYear;
    const make = req.params.manufacturer;
    const model = req.params.model;    

    // respond to malformed requests
    if ( isNaN(year) || !make || !model) {
        res.status(400).json({
            Count: 0,
            Results: []
        });
    }

    // fetch the data from the NHTSA API
    const url =
    `https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/${year}/make/${make}/model/${model}?format=json`;

    axios.get(url)
    .then((axiosResult) => {          

        // respond to zero results from the NHTSA API        
        if (axiosResult.data.Count === 0) {
            res.status(404).json({
                Count: 0,
                Results: []
            });
        }

        // respond to non zero results from the NHTSA API
        const results = [];
        createTheResultsArray(axiosResult.data, results);
        if (req.query.withRating === "true") {
            // respond to the "?withRating=true" requests
            const resultsWithRating = [];
            results.forEach((vehicle) => {

                // fetch the rating data for the specific VehicleId from the NHTSA API
                const vehicleWithRating = {};                                        
                const url = `https://one.nhtsa.gov/webapi/api/SafetyRatings/VehicleId/${vehicle.VehicleId}?format=json`;
                axios.get(url)
                .then((axiosResult) => {                    
                    vehicleWithRating.CrashRating = axiosResult.data.Results[0].OverallRating;  
                    vehicleWithRating.Description = vehicle.Description;
                    vehicleWithRating.VehicleId = vehicle.VehicleId;                        
                    resultsWithRating.push(vehicleWithRating);   
                })
                .then(() => {
                    // respond only after the data for all vehicles in the loop are fetched
                    if (resultsWithRating.length === results.length)
                    {                    
                        res.status(200).json({
                            Count: resultsWithRating.length,
                            Results: resultsWithRating
                        });
                    }
                })
                .catch(() => { res.status(404) });  

            });
        } else {   
            // respond to all the other non-"?withRating=true" requests        
            res.status(200).json({               
                Count: axiosResult.data.Count,
                Results: results
            });    
        }       

    })
    .catch(() => { res.status(404) });  // catch bad requests

});     
```

## 06 Documentation

* Create a document that contains request examples with Postman.

(https://documenter.getpostman.com/view/1169289/RWEfMKN9)
